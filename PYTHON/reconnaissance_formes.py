import cv2
import numpy as np
import imutils
import matplotlib.pyplot as plt


class ShapeDetector:
    def __init__(self):
        pass
    def detect(self, c):
        # initialize the shape name and approximate the contour
        mur = 5 # pas de mur identifie
        shape = "non identifiee"
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)
        
        #Condition sur la taille de l'objet pour choisir sa forme => evite d'identifier les bruits
        if 600 > peri > 200 :
            # if the shape is a triangle, it will have 3 vertices
            if len(approx) == 3:
                shape = "triangle"
                mur = 0
            
                
            # if the shape is a pentagon, it will have 5 vertices
            elif len(approx) == 5:
                shape = "pentagone"
                mur = 1
                
            elif len(approx) == 6:
                shape = "hexagone"
                mur = 2
            
            elif 10<= len(approx) <=12 :
                shape = "etoile"
                mur = 3
                
            # otherwise, we assume the shape is a circle
            else:
                shape = "cercle"
                
        # return the name of the shape
        return shape, mur
      
if __name__ == '__main__':

    # load the image and resize it to a smaller factor so that
    # the shapes can be approximated better
    image = cv2.imread('./images_test/violet_rose/test7.jpg')
  
   ####  Seuillage par couleur #######
   # test des differentes couleurs recuperee sur notre image pour faire les limites
    
    rgb_color = np.array([85, 58, 79], dtype=np.uint8)

    rgb_color1 = np.array([91, 49, 86], dtype=np.uint8)

    rgb_color2 = np.array([99, 53, 91], dtype=np.uint8)

    hsv_color = cv2.cvtColor(np.array([[rgb_color]]), cv2.COLOR_RGB2HSV)[0][0] # conversion en valeurs HSV
    hsv_color1 = cv2.cvtColor(np.array([[rgb_color1]]), cv2.COLOR_RGB2HSV)[0][0] # conversion en valeurs HSV
    hsv_color2 = cv2.cvtColor(np.array([[rgb_color2]]), cv2.COLOR_RGB2HSV)[0][0] # conversion en valeurs HSV

    # Afficher les valeurs HSV résultantes
    image_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    
    # plage de couleurs pour la couleur verte
    lower_color = np.array([124, 51, 55])
    upper_color = np.array([187, 148, 129])

    seuil = cv2.inRange(image_hsv, lower_color, upper_color)

    # Operations morphologiques pour remplir nos objets d'interets 
    # et eliminer les bruits
    iterations =6

    elem_cross = cv2.getStructuringElement(cv2.MORPH_CROSS,(5,3))
    elem_rect = cv2.getStructuringElement(cv2.MORPH_RECT,(5,2))
    dilate = cv2.dilate(seuil,elem_cross,iterations)
    erode = cv2.erode(dilate,elem_rect,iterations-2)


    # find contours in the thresholded image and initialize the
    # shape detector

    cnts = cv2.findContours(erode, cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts) #Normalise les contours pour itérer plus facilement

    # Creation des contours des formes
    #cv2.drawContours(src, [c], -1, (R, G, B), 2) -> le "-1" signifie qu'on dessine tous
    #les contours et le 2 est le choix du remplissage 
    contours = cv2.drawContours(image,cnts, -1, (0,255,0), 2)
    
    
    sd = ShapeDetector()
    compteur = 0
    # loop over the contours
    for c in cnts:
        compteur+=1
        M = cv2.moments(c)
        if (M["m00"] ==0) :
            M["m00"] =1
           
        # compute the center of the contour
        cX = int((M["m10"] / M["m00"]) )
        cY = int((M["m01"] / M["m00"]) )
        
        
        #detect shape from contour
        shape, wall = sd.detect(c)
        
   

        #Creation du masque pour recuperer les couleurs des formes
        mask = np.zeros(image.shape[:2], np.uint8)
        cv2.drawContours(mask, [c], -1,(255,255,255), -1)
        

        #Convertion de l'image BGR (specifique à openCV) en RGB
        imgRGB = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        
        #Affichage des formes reconnues sur l'image
        objLbl=shape
        
        textSize = cv2.getTextSize(objLbl,cv2.FONT_HERSHEY_SIMPLEX,0.5,2)[0]
        
        cv2.putText(image, objLbl, (int(cX-textSize[0]/2),int(cY+textSize[1]/2)), cv2.FONT_HERSHEY_SIMPLEX,1,(0,0,255), 2)
        
        #show image
        cv2.imshow("Detection", image)
     
    print("compteur : ",compteur)


    cv2.waitKey(0)        