const express = require('express');
const app = express();
const port = 3000;
const cors = require('cors');
const { spawn } = require('child_process');
const ip = process.argv[2];


app.get('/', function(req, res) {
  res.sendFile('/home/admin/projet_colombius/index.html');
  
});

app.use(cors({
    origin: 'http://${ip}:5500'
}));
app.use(express.json());

app.post('/key', (req, res) => {
    console.log(req.body);
    res.send('Data received');
});

app.listen(port, () => {
    console.log(`Ecoute sur http://${ip}:${port}`)
});

app.post('/run-python', (req, res) => {
    const pythonProcess = spawn('python', ['/home/admin/projet_colombius/PYTHON/move.py', req.body.key]);
    
    // Gérez les sorties du processus Python
    pythonProcess.stdout.on('data', (data) => {
      console.log(`${data}`);
    });
    
    pythonProcess.stderr.on('data', (data) => {
      console.error(`stderr: ${data}`);
    });
    
    // Gérez la fin du processus Python
    pythonProcess.on('close', (code) => {
      res.send(`Code python bien exécuté`);
    });
  });
  

